<?php

if ( ! defined( 'ABSPATH' ) ) { exit; }

?>
<div class="oktober-fest">
	<div id="webcam-error" class="webcam-error hide"></div>
	<div class="fest-section-wrapper" id="fest-section-wrapper">
		<div class="close-fest"><a href="#"></a></div>
		<div class="section1 active section">
			<div class="section1-inner">
				<div class="section1-left section1-block desktop_view">
					<img src="<?php echo OKTOBERFEST_URL.'public/img/section1-b.png'; ?>">
				</div>
				<div class="instruction-wrapper section1-block">
					<h1>PROST!</h1>
					<p>Do you have what it takes to be a legend? Fill your stein and get ready to prove it!</p>
					<ul class="fest-instruction">
						<li><span class="diagonal1"><img src="<?php echo OKTOBERFEST_URL.'public/img/Bullet.svg'; ?>"></span>Accept camera permission</li>
						<li><span class="diagonal1"><img src="<?php echo OKTOBERFEST_URL.'public/img/Bullet.svg'; ?>"></span>Hold your full stein/mug up for as long as you can!</li>
						<li><span class="diagonal1"><img src="<?php echo OKTOBERFEST_URL.'public/img/Bullet.svg'; ?>"></span>Take some selfies!</li>
						<li><span class="diagonal1"><img src="<?php echo OKTOBERFEST_URL.'public/img/Bullet.svg'; ?>"></span>Stop the timer!</li>
						<li><span class="diagonal1"><img src="<?php echo OKTOBERFEST_URL.'public/img/Bullet.svg'; ?>"></span>Select your overlay</li>
						<li><span class="diagonal1"><img src="<?php echo OKTOBERFEST_URL.'public/img/Bullet.svg'; ?>"></span>Brag to your friends!</li>
					</ul>
					<a href="#" class="start-timer fbtn" id="start-timer">START</a>				
				</div>
				<div class="section1-right section1-block desktop_view">
					<img src="<?php echo OKTOBERFEST_URL.'public/img/hand.png'; ?>">
				</div>
			</div>
		</div>
		
		<div class="section2 section">
			<div class="section2-inner">
				<div class="timer-block block">
					<div class="total-time-wrapper" style="display: none"></div>
					<div class="timer-clock">
						<span class="timelbl1" id="min"></span>
						<span class="timelbl1">:</span>
						<span class="timelbl1" id="sec"></span>
					</div>
					
					<div class="timer-reset-wrapper">
						<a href="#" class="timer-reset" id="timer-reset">Restart</a>
					</div>
					<a href="#" class="end-timer" id="end-timer">Stop</a>
				</div>
				
				<div class="camera-block block">
					<div class="camera-block-inner" id="my_camera">
					<span class="cam-text">Camera</span>
					</div>
					<a id="take-photo" class="btn-take-photo" href="#">Take Photo</a>
				</div>
				
				<div class="thumbnail-block block">
					<ul class="thumb-list">
						<li class="thumb-item"><span>1ST PIC!</span></li>
						<li class="thumb-item"><span>HOLD IT…</span></li>
						<li class="thumb-item"><span>THIS IS IT!</span></li>
						<li class="thumb-item"><span>LAST SHOT!</span></li>
					</ul>
				</div>
			</div>
			<div class="section2-btn-container">
				<a href="#" class="section2-next fbtn disabled">NEXT</a>
				<div id="spinner4" class="hide spinner center">
					<div class="bounce1"></div>
					<div class="bounce2"></div>
					<div class="bounce3"></div>
					<div class="bounce4"></div>
					<div class="bounce5"></div>
				</div>
			</div>
		</div>
		<div class="section3 section">
			<div class="section3-inner">
				<div class="slider-text-block block">
					<p class="text1">Choose your image and Frame</p>
					<div class="retake-container mobile_view">
						<a href="#" class="re-take-image">Retake Photos</a>
					</div>
					<select name="frame-select" class="frame-select custom-class" id="frame_select">
						<option value="frame-img1">Frame1</option>
						<option value="frame-img2">Frame2</option>
						<option value="frame-img3">Frame3</option>
					</select>
					
					<form id="fest_form" method="post" class="fest-fomr" enctype="multipart/form-data">

					<?php echo wp_nonce_field( 'fest_nonce_action', 'fest_nonce', true, false ); ?>
					<input type="hidden" id="fest-time" name="fest-time" value="" >
					<input type="hidden" name="submission" value="1" >
					<input type="hidden" id="fest_image" name="fest_image" value="" >
					
					<div class="field-group name">
						<input type="text" id="fest-name" name="fest-name" class="form-control custom-class" value="" autocomplete="on" placeholder="Name" required />
					</div>

					<div class="field-group email">
						<input type="email" name="fest-email" id="fest-email" class="form-control custom-class" value="" autocomplete="on" placeholder="Email" required />
					</div>

					<!--div class="field-group phone">
						<input type="tel" id="fest-phone" name="fest-phone" class="form-control" autocomplete="on" value="" placeholder="Phone">
					</div-->
					<div class="field-group acceptance">
						<div class="acceptance-terms-cond">
							<input type="checkbox" id="fest-accept" name="fest-accept" class="form-control custom-class" value="1" required>
							<label for="fest-accept"><span>I agree to this website storing my information for marketing purposes. Jack's Abby will never spam or sell information.</span></label>
						</div>
					</div>
					
				</form>
				<div class="retake-container desktop_view">
					<a href="#" class="re-take-image">Retake Photos</a>
				</div>
				
				<div class="section3-btn-container2 desktop_view">
					<a href="#" class="section3-next fbtn disabled">NEXT</a>
				</div>
				
				</div>
				
				<div class="slider-block block">
					<div class="frame">
					<img id="frame_img" data-url="<?php echo OKTOBERFEST_URL.'public/frame_img/'; ?>" src="<?php echo OKTOBERFEST_URL.'public/frame_img/frame-img1.png'; ?>">
					<span class="final-timing"></span>
					</div>
					<div class="slider-for">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
					
					<div class="slider-nav">
						<div class="slick-item"></div>
						<div class="slick-item"></div>
						<div class="slick-item"></div>
						<div class="slick-item"></div>
					</div>
				</div>
			</div>
			<div class="section3-btn-container">
				<a href="#" class="section3-next fbtn mobile_view disabled">NEXT</a>
				<div id="spinner2" class="hide spinner center">
					<div class="bounce1"></div>
					<div class="bounce2"></div>
					<div class="bounce3"></div>
					<div class="bounce4"></div>
					<div class="bounce5"></div>
				</div>
			</div>
		</div>
		
		<div class="section4 section">
			<div class="section4-inner">
				<p class="text1 mobile_view">Save and share</p>
				<div class="final-image-block block final_frame">
					<img id="final_img_frame" src="<?php echo OKTOBERFEST_URL.'public/frame_img/frame-img1.png'; ?>">
					<img id="selected_img" src="<?php echo OKTOBERFEST_URL.'public/img/slider-Image1.png'; ?>">
					<span class="final-timing"></span>
				</div>
				
				<div class="share-block block">
					<p class="text1 desktop_view">Save and share your image</p>

					<ul class="share-list">
						<li class="share-item"><a class="fbtn" id="share-social" href="#">Share on social</a></li>
						<li class="share-item"><a class="fbtn" download id="download-link" href="#">Download</a></li>
						<li class="share-item"><a class="fbtn" id="email-link" href="#">Email</a></li>
					</ul>
					<div id="spinner" class="hide spinner center">
						<div class="bounce1"></div>
						<div class="bounce2"></div>
						<div class="bounce3"></div>
						<div class="bounce4"></div>
						<div class="bounce5"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="fest-mail-popup" style="display: none">
		<div class="fest-mail-popup-inner">
			<div class="close-fest-popup"><a class="close-email" href="#"></a></div>
			<div class="popup-block">
				<p class="text2">Send email pop up</p>
				<form name="email-fest" method="post" id="email-fest">
					<input type="hidden" name="download_id" value=""/>
					<ul class="mail-list">
						<li class="mail-item"><input placeholder="Email" type="text" name="sendMail[]" class="mail-input"></li>
						<li class="mail-item"><input placeholder="Email" type="text" name="sendMail[]" class="mail-input"></li>
					</ul>
				</form>
				<div class="add-email-wrapper">
					<a href="#" class="add-email">+</a>
				</div>
				<div id="spinner3" class="hide spinner center">
					<div class="bounce1"></div>
					<div class="bounce2"></div>
					<div class="bounce3"></div>
					<div class="bounce4"></div>
					<div class="bounce5"></div>
				</div>
				<div class="email-btn-wrappre">
					<a class="popup-btn back-email fbtn" href="#">Back</a>
					<a class="popup-btn fbtn" id="send_mail" href="#">Send</a>
					<a class="popup-btn reset-fest hide fbtn" href="#">Restart</a>
				</div>
			</div>
		</div>
	</div>
	
	<div class="fest-share-popup" style="display: none">
		<div class="fest-share-popup-inner">
			<div class="close-fest-popup"><a class="close-social" href="#"></a></div>
			<form name="social_fest" method="post" id="social_fest">
			<?php echo wp_nonce_field( 'fest_nonce_action', 'fest_nonce', true, false ); ?>
				<input type="hidden" name="download_id" value=""/>
			</form>
			<div class="popup-block">
				<p class="text2">Share this page!</p>
				<div class="share-btn-container hide">
					<a class="share-btn facebook" href="#" target="_blank">
						<img class="share-img" src="<?php echo OKTOBERFEST_URL.'public/img/facebook.png'; ?>">
					</a>
					<a class="share-btn twitter" href="#" target="_blank">
						<img class="share-img" src="<?php echo OKTOBERFEST_URL.'public/img/twitter.png'; ?>">
					</a>
					<!-- <a class="share-btn instagram" href="#" target="_blank">
						<img class="share-img" src="<?php echo OKTOBERFEST_URL.'public/img/instagram.png'; ?>">
					</a> -->
				</div>
				<a class="popup-btn back-share fbtn" href="#">Back</a>
			</div>
		</div>
	</div>
	
</div>