(function( $ ) {
	'use strict';
	
	 $( window ).load(function() {
 
       // If AJAX Submission is disabled and the form has already been validated unblock the submit
	   if ( $("#fest_form").find(".is-invalid").length ) {     
		  $("#fest_form").addClass("was-validated");
		  $("#fest_form").removeClass("needs-validation");
       }    
       
       $( "input,textarea" ).on("input", function()  {
         $(this).val($(this).val().replace(/\s\s+/g, " "));
         var field = $(this).attr('id');    	 
	     if ( $(this).is(":valid") ) {   
		    $(this).removeClass("is-invalid");
            $('label[for="' + $(this).attr('id') + '"]').removeClass("is-invalid");
            $(this).next().children().removeClass("d-block");
            $(this).parent().removeClass("is-invalid");
            if( $("#fest_form").hasClass("needs-validation") ) { 
	          if ( ! $("#fest_form").find(":invalid").length ) {     
		         $('.message').removeClass("visible");
       	      } 
       	    } 
       	    else {     
		      if ( ! $("#fest_form").find(".is-invalid").not("#fest-error").length ) {  
		         $('.message').removeClass("visible");
       	      } 
       	    } 
            if ( $(this).prop('required') ) {
               $('label[for='+field+'] span').addClass("d-none"); 
            }
    	 }
    	 else {   
	       if ( $("#fest_form").hasClass("was-validated") ) {
		    $('label[for='+field+'] span').removeClass("d-none");
		    $(this).addClass("is-invalid");
            $('label[for="' + $(this).attr('id') + '"]').addClass("is-invalid");
            $(this).next().children().addClass("d-block");
            $(this).parent().addClass("is-invalid");
           }   
           if ( $(this).prop('required') ) {
               $('label[for='+field+'] span').removeClass("d-none"); 
           }

    	 }

       });

       $("#fest-consent").on("click",function () {
  	     if( $('#fest-consent').prop("checked") == false ) {
			$('#fest-consent').val('false'); 
		 } else { $('#fest-consent').val('true'); }
       });  
       
       $("#fest-captcha").focus(function(){
       $(this).parent().addClass("focus");
       }).blur(function(){
       $(this).parent().removeClass("focus");
       })      
       
       $( "#fest-captcha" ).on('input', function(e)  {
		  if ( $(this).is(":valid") ) {
			$('label[for="fest-captcha"] span').addClass("d-none");
            $(this).removeClass("is-invalid");                      
	        $("#captcha-question-wrap").removeClass("is-invalid");  
            $("#captcha-error span").removeClass("d-block");
	      }
       });
 
       // Prevent zero to be entered as first value in captcha field
       $("#sform-captcha").on("keypress", function(e) {
	     if (e.which === 48 && !this.value.length)
	     e.preventDefault();
	   });  
	   
       // Prevent space to be entered as first value
       $("input, textarea").on("keypress", function(e) {
	     if (e.which === 32 && !this.value.length)
	     e.preventDefault();
	   }); 
	   
	   
		var hr = 0;
		var min = 0;
		var sec = 0;
		var cycle;
		
		var vidWidth = 492; 
		var vidHeight = 600;

		var hour = document.getElementById("hour");
		var mint = document.getElementById("min");
		var secd = document.getElementById("sec");
	   
		function timerCycle() {
			
			//hour.innerHTML = '00';
            mint.innerHTML = '00';
            secd.innerHTML = '00';
			
			sec = parseInt(sec);
			min = parseInt(min);
			hr = parseInt(hr);

			sec = sec + 1;

			if (sec == 60) {
				min = min + 1;
				sec = 0;
			}
			if (min == 60) {
				hr = hr + 1;
				min = 0;
				sec = 0;
			}

			if (sec < 10 || sec == 0) {
				sec = '0' + sec;
			}
			if (min < 10 || min == 0) {
				min = '0' + min;
			}
			if (hr < 10 || hr == 0) {
				hr = '0' + hr;
			}

			localStorage.setItem('hr', hr);
			localStorage.setItem('min', min);
			localStorage.setItem('sec', sec);

			//hour.innerHTML = hr;
			mint.innerHTML = min;
			secd.innerHTML = sec;

			cycle = setTimeout(timerCycle, 1000);
		}

		$('#start-timer').on('click', function(e){
			e.preventDefault();
			localStorage.clear();
			hr = 0;
			min = 0;
			sec = 0;
			clearTimeout(cycle);
			
			$('.oktober-fest .section1').removeClass('active');
			$('.oktober-fest .section2').addClass('active');
			
			$("#start-timer").prop("disabled", true).html('Running');
			$('#end-timer').show();
			$('.timer-clock').show();
			$('.total-time-wrapper').hide();
			timerCycle();

		});
		
		$('#end-timer').on('click', function(e){
			e.preventDefault();
			localStorage.clear();
            //hour.innerHTML = '00';
            mint.innerHTML = '00';
            secd.innerHTML = '00';
            var total_time = min + ':' + sec;
			var total_time_html = '<span class="timelbl1">'+min+'</span> <span class="timelbl1">:</span> <span class="timelbl1">'+sec+'</span>';
			$(".total-time-wrapper").html(total_time_html);
           
            clearTimeout(cycle);
            hr = 0;
            min = 0;
            sec = 0;
            //$("#start-timer").prop("disabled", false).html('Start');
            $('.timer-clock').hide();
			$('.total-time-wrapper').show();
			//$('#show-form').show();
			$('input#fest-time').val(total_time);
			$(this).addClass('stopped');
			$('#take-photo').addClass('disabled');
			if($('.thumb-list .thumb-item:eq(0)').find('img').length){
				$('.section2-next').removeClass('disabled');
			}
		});
		
		$('#timer-reset').on('click', function(e){
			e.preventDefault();
			mint.innerHTML = '00';
            secd.innerHTML = '00';
           
            clearTimeout(cycle);
            hr = 0;
            min = 0;
            sec = 0;
			timerCycle();
			$('.timer-clock').show();
			$('.total-time-wrapper').hide();
			$('#end-timer').removeClass('stopped');
			$('.thumb-list .thumb-item').html('<span>Image Thumbnail</span>');
			$('#take-photo').removeClass('disabled');
		});
		
		var slickOptions2 = {
			slidesToShow: 4,
			slidesToScroll: 1,
			asNavFor: '.slider-for',
			dots: false,
			focusOnSelect: true,
			arrows: false,
			infinite: false,
		};
		
		var slickOptions1 = {
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: true,
			nextArrow: '<div class="slick-next"></div>',
			prevArrow: '<div class="slick-prev"></div>',
			asNavFor: '.slider-nav',
			infinite: false,
		};
		
		$('.slider-nav').slick(slickOptions2);
		$('.slider-for').slick(slickOptions1);
		
		$('.section2-next').on('click', function(e){
			e.preventDefault();
			if($(this).hasClass('disabled')){
				alert('Stop the timer before moving on!');
			}else if(!$('#end-timer').hasClass('stopped')){
				alert('Stop the timer before moving on!');
			}else{
				$('#spinner4').removeClass('hide');
				var final_timing = $('#fest-time').val();
				$('.final-timing').text(final_timing);
				setTimeout(function(){
					var sliderForTxt = "";
					var sliderNavTxt = "";
					$('.thumb-list li').each(function(){
						var imgTxt = $(this).find('img').attr('src');
						var imgtxtUrl = "";
						var userphoto = "";
						if(imgTxt != undefined ){
							imgtxtUrl = imgTxt;
							userphoto = 'user-photo';
						}else{
							imgtxtUrl = ajax_fest_submit.no_img;
							userphoto = 'user-no-image';
						}
						sliderForTxt += '<div><img src="'+ imgtxtUrl +'" class="'+ userphoto +'"></div>';
						sliderNavTxt += '<div class="slick-itetm"><img src="'+ imgtxtUrl +'" class="'+ userphoto +'"></div>';
					});
					
					if($(".slider-nav").hasClass("slick-initialized")){
						//$(".slider-nav").slick('unslick');
						$(".slider-nav").slick('destroy');
						$(".slider-nav").html("");
					}
					if($(".slider-for").hasClass("slick-initialized")){
						//$(".slider-for").slick('unslick');
						$(".slider-for").slick('destroy');
						$(".slider-for").html("");
					}
					
					$(".slider-nav").html(sliderNavTxt);
					$(".slider-for").html(sliderForTxt);
					
					$('.slider-nav').slick(slickOptions2);
					$('.slider-for').slick(slickOptions1);
					
					$('.slider-nav').resize();
					$('.slider-for').resize();
				 
					$('#spinner4').addClass('hide');
					$('.oktober-fest .section2').removeClass('active');
					$('.oktober-fest .section3').addClass('active');
				}, 1500);
			}
		});

		$('#fest_form input.custom-class').on('keyup change', function() {
			
			var empty = false;
			$('#fest_form input.custom-class').each(function() {
				if ($(this).val() == '') {
					empty = true;
				}

				if($(this).attr('id') == 'fest-accept'){
					if($(this).is(':not(:checked)')){
						empty = true;
					}				
				}
			});			
	
			if (empty) {
				$('.section3-next').addClass('disabled');
			} else {
				$('.section3-next').removeClass('disabled');
			}
		});
		
		$('.section3-next').on('click', function(e){
			e.preventDefault();
			var selectedPhoto = $('.slider-for .slick-current.slick-active').find('img').attr('src');
			$('.final-image-block #selected_img').attr('src', selectedPhoto);

			var isDataURL = /^\s*data:([a-z]+\/[a-z]+(;[a-z\-]+\=[a-z\-]+)?)?(;base64)?,[a-z0-9\!\$\&\'\,\(\)\*\+\,\;\=\-\.\_\~\:\@\/\?\%\s]*\s*$/i;

			if(!!selectedPhoto.match(isDataURL)){
				upload_webcam(selectedPhoto);
			}else{
				alert('Please select valid photo to proceed.');
			}			
		});
		
		$('#show-form').on('click', function(){
			$('.fest-form-wrapper').show();
		});
		
		$('#share-social').on('click', function(e){
			e.preventDefault();
			$('#spinner').removeClass('hide');
			var postdata = $('form#social_fest').serialize();
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url:ajax_fest_submit.ajaxurl, 
				data: postdata + '&action=formdata_social_media',
				success: function(data){
					$('#spinner').addClass('hide');
					$('.share-btn-container').removeClass('hide');	
					var facebookUrl = "https://www.facebook.com/sharer.php?u="+data+"&hashtags=BeTheLegend,CopperLegend";
					var twitterUrl = "https://twitter.com/share?url="+data+"&amp;text=I am a Legend! Check out my @JacksAbby stein hoist.&amp;hashtags=BeTheLegend,CopperLegend";
					// var instagramUrl = "https://www.instagram.com/?url="+data;
					$('.share-btn.facebook').attr('href', facebookUrl);	
					$('.share-btn.twitter').attr('href', twitterUrl);
					// $('.share-btn.instagram').attr('href', instagramUrl);

					$('.fest-section-wrapper').hide();
					$('.fest-share-popup'). show();
				},
				error: function(data){
					console.log(data);
					alert('Error getting share data');	            
				} 	
			});
		});
		
		$('#email-link').on('click', function(e){
			e.preventDefault();
			$('.fest-section-wrapper').hide();
			$('.fest-mail-popup'). show();
		});
		
		$('a.add-email').on('click', function(e){
			e.preventDefault();
			var inputTxt = '<li class="mail-item"><input placeholder="Email" type="text" name="sendMail[]" class="mail-input"></li>';
			$('.mail-list').append(inputTxt);
		});
		
		$('#send_mail').on('click', function(e){
			e.preventDefault();
			var allValid = true;
			var oneMail = false;
			$('.fest-mail-popup').find('.one-mail-error').remove();
			$('.fest-mail-popup').find('.success-msg').remove();
			$('.mail-item').find('.error').remove();
			
			$('.mail-input').each(function(){
				if($(this).val() != ""){
					oneMail = true;
					if(!isEmail($(this).val())){
						if(!$(this).parent().find('.error').length){
						$(this).after('<span class="error">Enter a valid Email.</span>');
						}
						allValid = false;
					}
				}
			});
			
			if(allValid && oneMail){
				$('#spinner3').removeClass('hide');
				var postdata = $('form#email-fest').serialize();
				$.ajax({
					type: 'POST',
					dataType: 'json',
					url:ajax_fest_submit.ajaxurl, 
					data: postdata + '&action=formdata_email',
					success: function(data){
						$('#spinner3').addClass('hide');
						$('.reset-fest').removeClass('hide');
						
						$('input.mail-input').val('');
						$('.fest-mail-popup').find('.one-mail-error').remove();
						$('.mail-item').find('.error').remove();
						if(!$('.fest-mail-popup').find('.success-msg').length){
							$('.add-email-wrapper').after('<span class="success-msg">'+data+'</span>');
						}
					},
					error: function(data){
						$('#spinner3').addClass('hide');
						alert('Error sending email');	            
					} 	
				});
			}else{
				if(!$('.fest-mail-popup').find('.one-mail-error').length){
				$('.add-email-wrapper').after('<span class="error one-mail-error">Enter atleast one valid Email.</span>');
				}
			}
		});
		
		
		$('.re-take-image').on('click', function(e){
			e.preventDefault();
			$('.oktober-fest .section3').removeClass('active');
			$('.oktober-fest .section2').addClass('active');
			$('#timer-reset').trigger('click');
			$('#take-photo').removeClass('disabled');
			$('.section2-next.fbtn').addClass('disabled');
		});
		
		$('.close-fest a').on('click', function(e){
			e.preventDefault();
			//$('.oktober-fest').hide();
			$('.reset-fest').trigger('click');
		});
		
		var device_width = (window.innerWidth > 0) ? window.innerWidth : screen.width;
		
		var camera_width = 450;
		var camera_height = 562;

		if(device_width < 768){
			camera_width = 320;
			camera_height = 400;
		}
		
		Webcam.set({
			width: camera_width,
			height: camera_height,
			
			constraints: {
				width: camera_width,
				height: camera_height,
			},

			video: {
				width: camera_width,
				height: camera_height
			},
						
			// final cropped size
			crop_width: camera_width,
			crop_height: camera_height,
			
			image_format: 'jpeg',
			jpeg_quality: 100
		});
		
		if($('#my_camera').length){
			Webcam.attach( '#my_camera' );
		}
		
		$('#take-photo').on('click', function(e){
			e.preventDefault();
			if($(this).hasClass('disabled')){
				return;
			}
			Webcam.snap( function(data_uri) {
				//if($('#end-timer').hasClass('stopped')){
					//$('.section2-next').removeClass('disabled');
				//}
				if(!$('.thumb-list .thumb-item:eq(0)').find('img').length){
					$('.thumb-list .thumb-item:eq(0)').html('<img src="'+data_uri+'"/>');
				}else if(!$('.thumb-list .thumb-item:eq(1)').find('img').length){
					$('.thumb-list .thumb-item:eq(1)').html('<img src="'+data_uri+'"/>');
				}else if(!$('.thumb-list .thumb-item:eq(2)').find('img').length){
					$('.thumb-list .thumb-item:eq(2)').html('<img src="'+data_uri+'"/>');
				}else if(!$('.thumb-list .thumb-item:eq(3)').find('img').length){
					$('.thumb-list .thumb-item:eq(3)').html('<img src="'+data_uri+'"/>');
				}
			});
			
		});
		
		var isEmail = function(email) {
			var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			return regex.test(email);
		}
		
		var validate_form = function(){
			var name = $('input#fest-name').val();			
			if (name == "") {
				if(!$('#fest-name').parent().find('.error').length){
					$('#fest-name').parent().append('<span class="error">Enter a valid name.</span>');
				}
				$('#fest-name').focus();
				return false;
			}else{
				$('#fest-name').parent().find('.error').remove();
			}
			
			var email = $('input#fest-email').val();
			if (email == "" || (!isEmail(email))) {
				if(!$('#fest-email').parent().find('.error').length){
					$('#fest-email').parent().append('<span class="error">Enter a valid Email.</span>');
				}
				$('#fest-email').focus();
				return false;
			}else{
				$('#fest-email').parent().find('.error').remove();
			}
			
			/*var phone = $('input#fest-phone').val();
			var phoneRegex = /[0-9 -()+]+$/;
			if((phone == "") || (phone.length < 6) || (!phoneRegex.test(phone))){
				if(!$('#fest-phone').parent().find('.error').length){
					$('#fest-phone').parent().append('<span class="error">Enter a valid Phone.</span>');
				}
				$('#fest-phone').focus();
				return false;
			}else{
				$('#fest-phone').parent().find('.error').remove();
			}*/
			
			var acceptance = $('input#fest-accept');
			if(acceptance.is(":not(:checked)")){
				if(!$('#fest-accept').parent().find('.error').length){
					$('#fest-accept').parent().append('<span class="error">Please check the acceptance term.</span>');
				}
				return false;
			}else{
				$('#fest-accept').parent().find('.error').remove();
			}
			return true;
		};
		
		var upload_webcam = function(data_uri){
			var totalTime = $('#fest-time').val();
			var imgFrame = $('#frame_select').val();
			var device_width_1 = (window.innerWidth > 0) ? window.innerWidth : screen.width;
			var uploadUrl = ajax_fest_submit.ajaxurl +'?action=fest_file_upload&timing='+ totalTime + '&frame='+imgFrame+ '&assign_device_width='+device_width_1;
			var validation = validate_form();
			if(validation){
				Webcam.upload( data_uri, uploadUrl, function(code, text) {
					$('input#fest_image').val(text);
					var download_img = ajax_fest_submit.upload_dir + text;
					$('#download-link').attr('href', download_img);
					var postdata = $('form#fest_form').serialize();
					$.ajax({
						type: 'POST',
						dataType: 'json',
						url:ajax_fest_submit.ajaxurl, 
						data: postdata + '&action=formdata_submit',
						success: function(data){
							if(data != 'error'){
								$('#spinner2').addClass('hide');
								$('.oktober-fest .section3').removeClass('active');
								$('.oktober-fest .section4').addClass('active');
								$('#share-social').attr('data-id', data);
								//$('#download-link').attr('data-id', data);
								$('#email-link').attr('data-id', data);
								$('input[name="download_id"]').val(data);
							}else{
								console.log('Error submitting data');
							}
						},
						error: function(data){
							$('#spinner2').addClass('hide');
							alert('error submitting form');
						} 	
					});
					
				});
			}else{
				$('#spinner2').addClass('hide');
			}
		};
		
		$('.back-email, .close-email').on('click', function(e){
			e.preventDefault();
			$('.fest-mail-popup').hide();
			$('.fest-section-wrapper').show();
		});
		
		$('.back-share, .close-social').on('click', function(e){
			e.preventDefault();
			$('.fest-share-popup').hide();
			$('.fest-section-wrapper').show();
		});
		
		$('.reset-fest').on('click', function(e){
			e.preventDefault();
			$('.fest-mail-popup').hide();
			$('.fest-section-wrapper').show();
			
			$('.fest-section-wrapper .section').removeClass('active');
			$('.fest-section-wrapper .section1').addClass('active');
			$('.final-timing').text('');
			$("#start-timer").prop("disabled", false).text('Start');
			$('.thumb-list .thumb-item').html('<span>Image Thumbnail</span>');
			$('#end-timer').removeClass('stopped');
			$('#take-photo').removeClass('disabled');
			$('.fest-mail-popup').find('.success-msg').remove();
			$("#fest_form")[0].reset();
			$("#email-fest")[0].reset();
			$("#social_fest")[0].reset();
			$('#timer-reset').trigger('click');
		});
		
		$('#frame_select').on('change', function(){
			var imgframe = $(this).val();
			var dataUrl = $('#frame_img').attr('data-url');
			var imgUrl = dataUrl + imgframe + ".png";
			$('#frame_img').attr('src', imgUrl);
			$('#frame_img').show();
			$('#final_img_frame').attr('src', imgUrl);
		});
		
   	 });

})( jQuery );