<?php
	
/**
 * Defines the public-specific functionality of the plugin.
 */

class Oktoberfest_Public {

	private $plugin_name;
	private $version;
	 
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	public function enqueue_styles() {
		
		wp_register_style( 'fest-slick-style', plugins_url('/css/slick.css',__FILE__)); 
		
		wp_register_style( 'fest-default-style', plugins_url('/css/default.css',__FILE__)); 
		
		wp_enqueue_style( 'fest-slick-style' );
		wp_enqueue_style( 'fest-default-style' );
    }
    
	public function enqueue_scripts() {
		
		wp_register_script( 'fest_slick_script', plugin_dir_url( __FILE__ ) . 'js/slick.min.js', array( 'jquery' ), $this->version, false );
		
		wp_register_script( 'fest_webcam_script', plugin_dir_url( __FILE__ ) . 'js/webcam.js', array( 'jquery' ), $this->version, false );
	
		wp_register_script( 'fest_form_script', plugin_dir_url( __FILE__ ) . 'js/script.js', array( 'jquery' ), $this->version, false );
		
		$upload = wp_upload_dir();
		$upload_dir = $upload['baseurl'];
		$upload_dir = $upload_dir . '/cam-img/';
		
		wp_localize_script('fest_form_script', 'ajax_fest_submit', array('ajaxurl' => admin_url('admin-ajax.php'), 'fest_loading_img' => plugins_url( 'img/processing.svg',__FILE__ ), 'upload_dir' => $upload_dir, 'no_img' => plugins_url( 'img/no-img.png',__FILE__ )));	
		
		wp_enqueue_script( 'fest_slick_script');
		wp_enqueue_script( 'fest_webcam_script');
		wp_enqueue_script( 'fest_form_script');
        wp_enqueue_script( 'fest_public_script');
    }

    public function fest_shortcode($atts) { 
	
		$file = "partials/default.php";

		include $file;
		
		$empty_data = "";
		
		$data = apply_filters( 'fest_form_validation', $empty_data );
		apply_filters( 'fest_download_image', $empty_data );
      
		$above_form = isset( $_GET['sending'] ) && $_GET['sending'] == 'success' ? '' : '<div id="sform-introduction" class=""></div>';
	  
		$below_form = isset( $_GET['sending'] ) && $_GET['sending'] == 'success' ? '' : '<div id="sform-bottom" class=""></div>';
    } 

	/**
	 * Validate the form data after submission without Ajax
	 */
	 
		 
    public function formdata_validation($data) {
		
        $name_requirement = '';
		
        $email_requirement = '';
			
        $phone_requirement = '';  
		
        $subject_requirement = '';
		
        $consent_requirement = ''; 
		
        $captcha_field = 'hidden';            
      
        if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submission']) && isset( $_POST['fest_nonce'] ) && wp_verify_nonce( $_POST['fest_nonce'], 'fest_nonce_action' ) ) {
	
			$formdata = array(
				'name' => isset($_POST['fest-name']) ? sanitize_text_field($_POST['fest-name']) : '',
				
				'email' => isset($_POST['fest-email']) ? sanitize_email($_POST['fest-email']) : '',
				
				'phone' => isset($_POST['fest-phone']) ? sanitize_text_field($_POST['fest-phone']) : '',
				
				'fest-time' => isset($_POST['fest-time']) ? sanitize_text_field($_POST['fest-time']) : '',
					
				'message' => isset($_POST['fest-message']) ? sanitize_textarea_field($_POST['fest-message']) : '',
				
				'image' => isset($_FILES["fest-photo"]["name"]) ? sanitize_textarea_field($_FILES["fest-photo"]["name"]) : '',
				
				'consent' => isset($_POST['fest-consent']) ? 'true' : 'false',
				
			);
			
			$error = '';		
			$name_length = '2';
			$name_regex = '#[0-9]+#';

			if ( $name_requirement == 'required' )	{
				if (  ! empty($formdata['name']) && preg_match($name_regex, $formdata['name'] ) ) { 
				$error .= 'name_invalid;';
				} else {
					if ( empty($formdata['name']) || strlen($formdata['name']) < $name_length ) {
					$error .= 'name;';
					}
				}		
			} else {	
				if (  ! empty($formdata['name']) && preg_match($name_regex, $formdata['name'] ) ) { 
					$error .= 'name_invalid;';
				} else {
					if ( ! empty($formdata['name']) && strlen($formdata['name']) < $name_length ) {
						$error .= 'name;';
					}         
				}
			}

			$data_name = $formdata['name'];
			
			if ( $email_requirement == 'required' )	{
				if ( empty($formdata['email']) || ! is_email($formdata['email']) ) {
				$error .= 'email;';
				}
			} else {		
				if ( ! empty($formdata['email']) && ! is_email($formdata['email']) ) {
				$error .= 'email;';
				}
			}		

			$data_email = $formdata['email'];

			$phone_regex = '/^[0-9\-\(\)\/\+\s]*$/';  // allowed characters: -()/+ and space

			if ( $phone_requirement == 'required' )	{
				if (  ! empty($formdata['phone']) && ! preg_match($phone_regex, $formdata['phone'] ) ) { 
					$error .= 'phone_invalid;';
				} else {		
					if ( empty($formdata['phone']) ) {
						$error .= 'phone;';
					}
				}		
			} else {		
				if (  ! empty($formdata['phone']) && ! preg_match($phone_regex, $formdata['phone'] ) ) { 
					$error .= 'phone_invalid;';
				}
			}		

			$data_phone = $formdata['phone'];

			$message_length = isset( $attributes['$message_minlength'] ) ? esc_attr($attributes['$message_minlength']) : '10';
			$message_regex = '/^[^#$%&=+*{}|<>]+$/';

			if (  ! empty($formdata['message']) && ! preg_match($message_regex, $formdata['message'] )  ) { 
				$error .= 'message_invalid;';
			} else {		
				if ( strlen($formdata['message']) < $message_length ) {
					$error .= 'message;';
				}
			}

			$data_message = $formdata['message'];		

			if ( $consent_requirement == 'required' && $formdata['consent'] !=  "true" ) {
				$error .= 'consent;'; 
			}
			$data_consent = $formdata['consent'];
			
			$error = apply_filters('fest_submit_form', $formdata, $error );
			
			$data = array( 'name' => $data_name,'email' => $data_email,'phone' => $data_phone,'message' => $data_message,'consent' => $data_consent,'error' => $error );

	    } else {	
			$data = array( 'name' => '','email' => '','phone' =>'','message' => '','consent' => '' );
		}
		
        return $data;
	}

	/**
	 * Modify the HTTP response header (buffer the output so that nothing gets written until you explicitly tell to do it)
	 *
	 * @since    1.8.1
	 */
    
    public function ob_start_cache($error) {
		if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submission']) && isset( $_POST['fest_nonce'] ) && wp_verify_nonce( $_POST['fest_nonce'], 'fest_nonce_action' ) ) {
			if ($error == '') {
				ob_start();
			}
		}
    }

	/**
	 * Process the form data after submission with post callback function
	 */
	 
	public function formdata_email(){
		if( 'POST' !== $_SERVER['REQUEST_METHOD'] ) { 
		die ( 'Security checked!'); 
		} else {
			$id = $_POST['download_id'];
			$upload = wp_upload_dir();
			$upload_dir = $upload['baseurl'];
			$upload_dir = $upload_dir . '/cam-img/';
			
			global $wpdb;
			$table_name = "{$wpdb->prefix}fest_submissions"; 
			$table_row = $wpdb->get_row("SELECT * FROM $table_name WHERE id = $id");
			
			$imgUrl = $upload_dir . $table_row->image;
			
			foreach ($_POST['sendMail'] as $email) {
				// sanitize form values
				$name    = '';
				$to  	 = sanitize_email($email);
				$subject = 'Proof That You\'re A Legend, From Filed: "Jack\'s Abby"';
				$message = '<!DOCTYPE html>
				<html lang="en" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
				<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
				<meta name="viewport" content="width=device-width,initial-scale=1" />
				<meta name="x-apple-disable-message-reformatting" />
				<title>Jack\'s Abby Craft Lagers</title>
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="format-detection" content="telephone=no" />
				<meta name="format-detection" content="address=no">
				<!--[if mso]>
					<xml>
					  <o:OfficeDocumentSettings>
						<o:AllowPNG/>
						<o:PixelsPerInch>96</o:PixelsPerInch>
					  </o:OfficeDocumentSettings>
					</xml>
					<style>
					  .spacer, .divider {mso-line-height-rule: exactly;}
					  td,th,div,p,a,h1,h2,h3,h4,h5,h6 {font-size:13px; line-height:23px; font-family:Helvetica,Arial,sans-serif;}
					</style>
					<![endif]-->
				
				<style type="text/css">
				@media only screen {
				.sans-serif {
					font-family: \'Helvetica\', Arial, arial, sans-serif!important;
				}
				.column, th, td, div, p {
					font-family: \'Helvetica\', Arial, arial, sans-serif!important;
				}
				}
				#outlook a {
					padding: 0;
				}
				a {
					text-decoration: none;
				}
				table {
					border-collapse: collapse;
				}
				img {
					border: 0;
					display: block;
					line-height: 100%;
				}
				.column, th, td, div, p {
					font-size: 14px;
					line-height: 22px;
				}
				.wrapper {
					min-width: 600px;
				}
				.row {
					margin: 0 auto;
					width: 600px;
				}
				.row .row, th .row {
					width: 100%;
				}
				.hover-shrink:hover {
					transform: scale(0.7);
				}
				
				@media only screen and (max-width: 599px) {
				.wrapper {
					min-width: 100% !important;
				}
				.row {
					width: 90% !important;
				}
				.row .row {
					width: 100% !important;
				}
				.column {
					box-sizing: border-box;
					display: inline-block !important;
					line-height: inherit !important;
					width: 100% !important;
					word-break: break-word;
					-webkit-text-size-adjust: 100%;
				}
				.mobile-1 {
					max-width: 8.33333%;
				}
				.mobile-2 {
					max-width: 16.66667%;
				}
				.mobile-3 {
					max-width: 25%;
				}
				.mobile-4 {
					max-width: 33.33333%;
				}
				.mobile-5 {
					max-width: 41.66667%;
				}
				.mobile-6 {
					max-width: 50%;
				}
				.mobile-7 {
					max-width: 58.33333%;
				}
				.mobile-8 {
					max-width: 66.66667%;
				}
				.mobile-9 {
					max-width: 75%;
				}
				.mobile-10 {
					max-width: 83.33333%;
				}
				.mobile-11 {
					max-width: 91.66667%;
				}
				.mobile-12 {
					padding-right: 20px !important;
					padding-left: 20px !important;
				}
				.mobile-offset-1 {
					margin-left: 8.33333% !important;
				}
				.mobile-offset-2 {
					margin-left: 16.66667% !important;
				}
				.mobile-offset-3 {
					margin-left: 25% !important;
				}
				.mobile-offset-4 {
					margin-left: 33.33333% !important;
				}
				.mobile-offset-5 {
					margin-left: 41.66667% !important;
				}
				.mobile-offset-6 {
					margin-left: 50% !important;
				}
				.mobile-offset-7 {
					margin-left: 58.33333% !important;
				}
				.mobile-offset-8 {
					margin-left: 66.66667% !important;
				}
				.mobile-offset-9 {
					margin-left: 75% !important;
				}
				.mobile-offset-10 {
					margin-left: 83.33333% !important;
				}
				.mobile-offset-11 {
					margin-left: 91.66667% !important;
				}
				.has-columns {
					padding-right: 20px !important;
					padding-left: 20px !important;
				}
				.has-columns .column {
					padding-right: 10px !important;
					padding-left: 10px !important;
				}
				.mobile-collapsed .column {
					padding-left: 0 !important;
					padding-right: 0 !important;
				}
				img {
					width: 100% !important;
					height: auto !important;
				}
				.mobile-center {
					display: table !important;
					float: none;
					margin-left: auto !important;
					margin-right: auto !important;
				}
				.mobile-left {
					float: none;
					margin: 0 !important;
				}
				.mobile-text-center {
					text-align: center !important;
				}
				.mobile-text-left {
					text-align: left !important;
				}
				.mobile-text-right {
					text-align: right !important;
				}
				.show-on-mobile {
					display: table !important;
					overflow: visible !important;
					line-height: inherit !important;
				}
				.hide-on-mobile {
					display: none !important;
					width: 0;
					overflow: hidden;
				}
				.mobile-full-width {
					display: table;
					width: 100% !important;
				}
				.mobile-first {
					display: table-header-group !important;
				}
				.mobile-intermediate {
					display: table-row !important;
				}
				.mobile-last {
					display: table-footer-group !important;
				}
				.mobile-first th, .mobile-intermediate th, .mobile-last th {
					padding-left: 30px !important;
					padding-right: 30px !important;
				}
				.h1 {
					font-size: 28px !important;
					line-height: 32px !important;
					margin-bottom: 10px;
				}
				.h2 {
					font-size: 22px !important;
					line-height: 30px !important;
				}
				.h3 {
					font-size: 16px !important;
				}
				.spacer {
					height: 30px;
					line-height: 100% !important;
					font-size: 100% !important;
				}
				.divider th {
					height: 60px;
				}
				.mobile-padding-top {
					padding-top: 30px !important;
				}
				.mobile-padding-top-mini {
					padding-top: 10px !important;
				}
				.mobile-padding-bottom {
					padding-bottom: 30px !important;
				}
				.mobile-padding-bottom-mini {
					padding-bottom: 10px !important;
				}
				.mobile-margin-top {
					margin-top: 30px !important;
				}
				.mobile-margin-top-mini {
					margin-top: 10px !important;
				}
				.mobile-margin-bottom {
					margin-bottom: 30px !important;
				}
				.mobile-margin-bottom-mini {
					margin-bottom: 10px !important;
				}
				.no-border-on-mobile {
					border: none !important;
				}
				}
				</style>
				</head>
				<body style="box-sizing:border-box;margin:0;padding:0;width:100%;-webkit-font-smoothing:antialiased;background-color:#efefef;">
				
				<!-- Email Wrapper -->
				<table class="wrapper" align="center" bgcolor="#efefef" cellpadding="0" cellspacing="0" width="100%">
				  <tr>
					<td style="padding: 10px 0;">
						<!-- Pre-Header -->
						<table class="row" align="center" cellpadding="0" cellspacing="0">
				  <tr valign="top" style="vertical-align: top;">
					<th class="column mobile-8 sans-serif" style="font-weight: 400; text-align: left;">
					  <div class="sans-serif" style="font-size:12px;line-height:24px;color:#858585;">You\'re a Legend baby.</div>
					</th>
				  </tr>
				</table>
													<!-- END Pre-Header -->
					  <table class="row" align="center" bgcolor="#f7d7a7" cellpadding="0" cellspacing="0">
							   <!-- HEADER -->
				  <tr valign="top" style="vertical-align: top;" class="mobile-collapsed" bgcolor="#722C17" >
					<th class="column" width="600"><a href="https://copperlegend.com/?utm_source=stein-hoist&utm_campaign=copperlegend&utm_medium=email"><img src="https://jacksabby.com/wp-content/uploads/2021/07/copper_legend_em_head.png" width="600" height="215"  alt="Copper Legend | BE THE LEGEND" style="border: 0; width: 100%; max-width: 600px;height:auto;color:#FFF!important;"></a>
					  </th>
				  </tr>
					  <!-- /HEADER  --> 
						  <tr>
						  <td class="spacer" height="30" style="line-height:30px; font-size:30px; mso-line-height-rule: exactly;">&nbsp;</td>
						</tr>
						 <tr>
						  <td class="column mobile-12 mobile-text-center" width="540" style="padding-left: 30px; padding-right: 30px; text-align: center;color:#333;">
							<div class="sans-serif" style="margin: 0 0 10px;font-size:24px;line-height:32px;color:#722C17!important; "><strong>You\'re a Legend! </strong></div>
							  <div class="sans-serif" style="margin: 0 0 30px;font-size:18px;line-height:24px;color:#722C17!important; ">Thanks for raising your glass with us. Prost!</div>
				<img src="'. $imgUrl .'" width="540" height="674"  alt="" style="border: 0; width: 100%; max-width: 540px;height:auto;">
						<div class="sans-serif" style="margin: 30px auto;font-size:14px;line-height:20px;text-align:center;">
						<center><table border="0" cellspacing="0" cellpadding="0"><tbody><tr>
						  <td style="padding: 11px 27px; border-radius: 0px; font-size:16px; color:#002855; font-family:\'Open Sans\', Arial, sans-serif; text-align:left; min-width:auto !important; font-weight:bold; line-height: 20px;" bgcolor="#ff9e18"><a href="https://copperlegend.com/?utm_source=stein-hoist&utm_campaign=copperlegend&utm_medium=email" target="_blank" style="text-decoration:none; color:#002855;"><span style="text-decoration:none; color:#002855;">HOIST YOUR STEIN AGAIN!</span></a></td></tr></tbody></table></center></div>
					  </td>    
						</tr>
						   <tr>
						  <td class="spacer" height="10" style="line-height: 10px; font-size:10px; mso-line-height-rule: exactly;">&nbsp;</td>
						</tr>
					  </table>
					<!-- /TOP COPY --> 
					  <!-- Footer -->
					  <table class="row" align="center" bgcolor="#722C17" cellpadding="0" cellspacing="0">
						<tr>
						  <th class="column has-columns" width="540" style="padding-left: 30px; padding-right: 30px;">
							  <table width="540" class="row" cellpadding="0" cellspacing="0">
							  <tr>
								<td class="spacer" height="30" colspan="2" style="font-size: 30px; line-height: 30px; mso-line-height-rule: exactly;">&nbsp;</td>
							  </tr>
							  <tr>
								<td class="column" width="475" style="color: #FFFFFF; font-weight: 400; text-align: center;">
									<center><table border="0" cellspacing="0" cellpadding="0">
				
																			<tbody><tr>
				
																	<td colspan="7" class="mobile-text-center" style="font-size:0pt; line-height:0pt; text-align:center; padding-bottom: 28px;">
				
																		<a href="https://jacksabby.com/?utm_source=stein-hoist&utm_campaign=copperlegend&utm_medium=email" target="_blank"><img src="https://jacksabby.com/wp-content/uploads/2015/09/JA-logo-white-200.png" width="200" height="71" border="0" alt="Jack\'s Abby" style="max-width:200px;"></a>
				
																	</td>
				
																</tr><tr>
				
																				<td class="img" width="25" style="font-size:0pt; line-height:0pt; text-align:left;">
				
																					<a href="https://www.facebook.com/jacksabbycraftlagers/?utm_source=stein-hoist&utm_campaign=copperlegend&utm_medium=email" target="_blank"><img src="https://jacksabby.com/wp-content/uploads/2016/12/social-fb-white.png" width="25" height="25" border="0" alt="Facebook" style="max-width:25px;"></a>
				
																				</td>
				
																				<td class="img" width="33" style="font-size:0pt; line-height:0pt; text-align:left;">&nbsp;</td>
				
																				<td class="img" width="25" style="font-size:0pt; line-height:0pt; text-align:left;">
																																		
																					<a href="https://twitter.com/jacksabby/?utm_source=stein-hoist&utm_campaign=copperlegend&utm_medium=email" target="_blank"><img src="https://jacksabby.com/wp-content/uploads/2016/12/social-tw-white.png" width="25" height="25" border="0" alt="Twitter" style="max-width:25px;"></a>
				
																				</td>
				
																				<td class="img" width="33" style="font-size:0pt; line-height:0pt; text-align:left;">&nbsp;</td>
				
																				<td class="img" width="25" style="font-size:0pt; line-height:0pt; text-align:left;">
				
																					<a href="https://instagram.com/jacksabbycraftlagers/?utm_source=stein-hoist&utm_campaign=copperlegend&utm_medium=email" target="_blank"><img src="https://jacksabby.com/wp-content/uploads/2016/12/social-insta-white.png" width="25" height="25" border="0" alt="Instagram" style="max-width:25px;"></a>
				
																				</td>
				
																				<td class="img" width="33" style="font-size:0pt; line-height:0pt; text-align:left;">&nbsp;</td>
				
																				<td class="img" width="25" style="font-size:0pt; line-height:0pt; text-align:left;">
				
																					<a href="https://untappd.com/jacksabbybrewing/?utm_source=stein-hoist&utm_campaign=copperlegend&utm_medium=email" target="_blank"><img src="https://jacksabby.com/wp-content/uploads/2016/12/social-untappd-white.png" width="25" height="25" border="0" alt="Untappd" style="max-width:25px;"></a>
				
																				</td>
				
																			</tr>
				
																		</tbody></table>
									</center>
									<div class="sans-serif" style="margin-top:20px;font-size:13px;line-height:22px;">Copyright &copy; 2021 Jack\'s Abby Brewing, LLC. All rights reserved.<br>
									100 Clinton St, Framingham, MA 01702<br>
									(508) 872-0900 <br>
									<br>
									copperlegend.com &nbsp;|&nbsp; jacksabby.com
									&nbsp;|&nbsp; <a href="https://copperlegend.com/privacy-policy/?utm_source=stein-hoist&utm_campaign=copperlegend&utm_medium=email" style="color:#FFF!important; text-decoration:none !important;">Privacy Policy</a>
									</div></td>
							  </tr>
							  <tr>
								<td class="spacer" height="30" colspan="2" style="font-size: 30px; line-height: 30px; mso-line-height-rule: exactly;">&nbsp;</td>
							  </tr>
								 
							</table>
						  </th>
						</tr>
					  </table>
					  
					  <!-- /Footer --></td>
				  </tr>
				</table>
				</body>
				</html>';

				// get the blog administrator's email address
				$admin = get_option( 'admin_email' );

				$headers = "From: Jacks Abby <copperlegend@jacksabby.com>" . "\r\n";
				$headers = "Reply-To: No Reply <no-reply@jacksabby.com>" . "\r\n";
				$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

				// If email has been process for sending, display a success message
				wp_mail( $to, $subject, $message, $headers );
			}
			
			print json_encode( 'The email has been sent successfully. Thank you.');
			wp_die();		
		}
	}
	
	public function formdata_social_media(){
		if( 'POST' !== $_SERVER['REQUEST_METHOD'] ) { 
		die ( 'Security checked!'); 
		} elseif ( ! wp_verify_nonce( $_POST['fest_nonce'], 'fest_nonce_action' ) )  {
			die ( 'Security checked!'); 
		} else {
			$id = $_POST['download_id'];
			$upload = wp_upload_dir();
			$upload_dir = $upload['baseurl'];
			$upload_dir = $upload_dir . '/cam-img/';
			
			global $wpdb;
			$table_name = "{$wpdb->prefix}fest_submissions"; 
			$table_row = $wpdb->get_row("SELECT * FROM $table_name WHERE id = $id");
			
			$imgUrl = $upload_dir . $table_row->image;
			
			print json_encode($imgUrl); wp_die();
		}
	}
	
	public function fest_file_upload(){
		if( $_SERVER['REQUEST_METHOD'] == 'POST'){
			$name = date('YmdHis');
			
			$upload = wp_upload_dir();
			$upload_dir = $upload['basedir'];
			$upload_dir = $upload_dir . '/cam-img';

			$imagename =  $name.".jpg";
			$imagePath =  $upload_dir."/".$imagename;
			$fileName = $name.".jpg";
			


			if($_REQUEST['timing']){
				$timing = $_REQUEST['timing'];
			}else{
				$timing = "00:00";
			}
			
			if($_REQUEST['frame']){
				$frame = $_REQUEST['frame'];
			}else{
				$frame = "frame";
			}

			$image_width = "";

			if($_REQUEST['assign_device_width']){
			
				$device_width = $_REQUEST['assign_device_width'];
				if($device_width < 768){
					$image_width = "320";
				}
			}else{
				$device_width = 768;
			}
			
			
			//$frame_name = "frame1.png";
			//$frame_name = "overlay-1.png";
			if ($image_width == "320"){			
				$frame_name = $frame."_".$image_width.".png";
			}else{
				$frame_name = $frame.".png";
			}			

			$frame_dir = plugin_dir_path( dirname( __FILE__ ) )."public/frame_img/".$frame_name;

			move_uploaded_file($_FILES['webcam']['tmp_name'], $imagePath);
			
			$border_width = 110;
			$border_height = 110;
			
			$retrun_file_name = $this->frame_image($frame_dir, $imagePath, $border_width, $border_height, $imagename, $timing, $device_width);
			
			echo $retrun_file_name; 
			wp_die();
		}
	}
	
	public function frame_image($border, $image, $borderW, $borderH, $newImageName, $text, $device_width){
		
		$png = imagecreatefrompng($border);
		$jpeg = imagecreatefromjpeg($image);
		
		$newimg = $image;

		list($width, $height) = getimagesize($image);
		list($newwidth, $newheight) = getimagesize($border);

		if($device_width < 768){
			$ddwidth = "320";
			$ddheight = "400"; 

			$out = imagecreatetruecolor($ddwidth, $ddheight);
				
			//new code for center image
			$sourceLeft = 0;
			$sourceTop = 0;
			
			if($newwidth > $width ){
				$sourceLeft = ($newwidth - $width) / 2;
				$width = $newwidth;
			}
			
			if($newheight > $height ){
				$sourceTop = ($newheight - $height) / 2 ;
				$height = $newheight;
			}
						
			$fontcolor = imagecolorallocate($out, 183, 64, 38);		
			imagecolortransparent($out, $fontcolor);
			imagealphablending($out, false);
			imagesavealpha($out, true);
			
			$final = imagecreatetruecolor($ddwidth, $ddheight);
			//New code end 
			$sourceLeft = "0";
			$sourceTop = "0";
			$ddsourceLeft = "0";
			$ddsourceTop = "0";
			// $ddwidth = "655";
			// $ddheight = "801"; 
			
			imagecopyresized($final, $jpeg,0,0,0,0, $ddwidth, $ddheight, $ddwidth, $ddheight);
			imagecopyresized($final, $png,0,0,0,0, $ddwidth, $ddheight, $ddwidth, $ddheight);

			imagecopyresampled($final, $jpeg, $sourceLeft, $sourceTop, $ddsourceLeft, $ddsourceTop, $ddwidth, $ddheight, $ddwidth, $ddheight);
			imagecopyresampled($final, $png, 0, 0, 0, 0, $ddwidth, $ddheight, $ddwidth, $ddheight);
			
			$textLeft = ($ddwidth / 2) - 50;
			$textBottom = $ddheight - 20;

		}else{
			$ddwidth = "655";
			$ddheight = "818";

			$out = imagecreatetruecolor($newwidth, $newheight);
				
			//new code for center image
			$sourceLeft = 0;
			$sourceTop = 0;
			
			if($newwidth > $width ){
				$sourceLeft = ($newwidth - $width) / 2;
				$width = $newwidth;
			}
			
			if($newheight > $height ){
				$sourceTop = ($newheight - $height) / 2 ;
				$height = $newheight;
			}
						
			$fontcolor = imagecolorallocate($out, 183, 64, 38);		
			imagecolortransparent($out, $fontcolor);
			imagealphablending($out, false);
			imagesavealpha($out, true);
			
			$final = imagecreatetruecolor($width, $height);
			//New code end 
			$sourceLeft = "0";
			$sourceTop = "0";
			$ddsourceLeft = "0";
			$ddsourceTop = "0";
			imagecopyresampled($final, $jpeg, $sourceLeft, $sourceTop, $ddsourceLeft, $ddsourceTop, $ddwidth, $ddheight, $width, $height);
			imagecopyresampled($final, $png, 0, 0, 0, 0, $width, $height, $width, $height);
			
			$textLeft = ($width / 2) - 50;
			$textBottom = $height - 45;
		}		

		
		$font =  plugin_dir_path( dirname( __FILE__ ) )."public/frame_img/"."Basetica-Medium.ttf";
		imagettftext($final, 30, 0, $textLeft, $textBottom, $fontcolor, $font, $text);

		imagejpeg($final, $newimg, 100);
				
		return $newImageName;
	}
	
	public function formdata_download(){
		if (isset( $_POST['download-submitted'] ) ){ 
			
			if ( ! empty($_POST['download_id']) ) { 
				$id = $_POST['download_id'];
				
				global $wpdb;
				$table_name = "{$wpdb->prefix}fest_submissions"; 
				$table_row = $wpdb->get_row("SELECT * FROM $table_name WHERE id = $id");
				
				$filepath = dirname( __FILE__ ) . $table_row->image;
				ob_end_flush();
				if(file_exists($filepath)) {
					header('Content-Description: File Transfer');
					header('Content-Type: application/octet-stream');
					header('Content-Disposition: attachment; filename="'.$filepath.'"');
					header('Expires: 0');
					header('Cache-Control: must-revalidate');
					header('Pragma: public');
					header('Content-Length: ' . filesize($filepath));
					flush(); 
					readfile($filepath);
					ob_end_flush();
					exit(); 
				} 
			}	
		}
	}
		 
	public function formdata_submit(){
		if( 'POST' !== $_SERVER['REQUEST_METHOD'] ) { 
			die ( 'Security checked!'); 
		} elseif ( ! wp_verify_nonce( $_POST['fest_nonce'], 'fest_nonce_action' ) )  {
			die ( 'Security checked!'); 
		} else {	
			$submission_timestamp = time();
			$submission_date = date('Y-m-d H:i:s');
  	            
			if ( ! empty($_POST['fest-name']) ) { 
				$name_value = $_POST['fest-name'];
			}else{
				$name_value = '';
			}
				
			if ( ! empty($_POST['fest-email']) ) { 
				$email_value = $_POST['fest-email'];
			} else {
				$email_value = '';
			}
				
			if ( ! empty($_POST['fest-phone']) ) { 
				$phone_value = $_POST['fest-phone'];
			} else {
				$phone_value = '';       
			}
			
			if ( ! empty($_POST['fest-time']) ) { 
				$total_time = $_POST['fest-time'];
			} else {
				$total_time = '';       
			}
			
			if ( ! empty($_POST['fest_image']) ) { 
				$imageurl = $_POST['fest_image'];
			} else {
				$imageurl = '';       
			}

			$flagged = '';

			global $wpdb;
			$table_name = "{$wpdb->prefix}fest_submissions"; 
		   
			$fest_default_values = array( "name" => $name_value, "phone" => $phone_value, "email" => $email_value, "total_time" => $total_time, "image" => $imageurl, "date" => $submission_date);
			
			
			$success = $wpdb->insert($table_name, $fest_default_values);
			
			if ($success)  {
				$this_insert = $wpdb->insert_id; 
				echo $this_insert;	 
			} else {
				echo "error";	 
			}
			wp_die();
		}
	}

    public function formdata_processing($formdata, $error) {  

		if( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['submission']) && isset( $_POST['fest_nonce'] ) && wp_verify_nonce( $_POST['fest_nonce'], 'fest_nonce_action' ) ) {
			
			$submission_timestamp = time();
			$submission_date = date('Y-m-d H:i:s');
  	            
			if ( ! empty($formdata['name']) ) { 
				$name_value = $formdata['name'];
			}else{
				$name_value = '';
			}
				
			if ( ! empty($formdata['email']) ) { 
				$email_value = $formdata['email'];
			} else {
				$email_value = '';
			}
				
			if ( ! empty($formdata['phone']) ) { 
				$phone_value = $formdata['phone'];
			} else {
				$phone_value = '';       
			}
			
			if ( ! empty($formdata['fest-time']) ) { 
				$total_time = $formdata['fest-time'];
			} else {
				$total_time = '';       
			}
			
			if ( ! empty($formdata['message']) ) { 
				$fest_message = $formdata['message'];
			} else {
				$fest_message = '';       
			}
			if ( ! function_exists( 'wp_handle_upload' ) ) 
				require_once( ABSPATH . 'wp-admin/includes/file.php' );
			
			$uploadedfile = $_FILES['fest-photo'];
			$upload_overrides = array( 'test_form' => false );

			$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
			$imageurl = "";
			
			if ( $movefile && ! isset( $movefile['error'] ) ) {
			   $imageurl = $movefile['url'];
			}else{
				$imageurl = $movefile['url'];
			}
			
		if ($error == '') {
			$flagged = '';

			global $wpdb;
			$table_name = "{$wpdb->prefix}fest_submissions"; 
		   
			$fest_default_values = array( "name" => $name_value, "phone" => $phone_value, "email" => $email_value, "total_time" => $total_time, "image" => $imageurl, "date" => $submission_date, "fest_message" => $fest_message);
			
			
			$success = $wpdb->insert($table_name, $fest_default_values);
			
			if ($success)  {

			$from_data = '<b>'. esc_html__('From', 'simpleform') .':</b>&nbsp;&nbsp;';
			$from_data .= $name_value;
			
			if ( ! empty($email_value) ):
			$from_data .= '&nbsp;&nbsp;&lt;&nbsp;' . $email_value . '&nbsp;&gt;';
			else:
			$from_data .= '';
			endif;
			$from_data .= '<br>';
			
			if ( ! empty($phone_value) ) { $phone_data = '<b>'. esc_html__('Phone', 'simpleform') .':</b>&nbsp;&nbsp;' . $phone_value .'<br>'; }
			else { $phone_data = ''; }
			$from_data .= $phone_data;
			
			if ( ! empty($subject_value) ) { $subject_data = '<br><b>'. esc_html__('Subject', 'simpleform') .':</b>&nbsp;&nbsp;' . $flagged . $subject_value .'<br>'; }
			else { 
				  if ( !empty($flagged)) { $subject_data = '<br><b>'. esc_html__('Subject', 'simpleform') .':</b>&nbsp;&nbsp;' . $flagged .'<br>'; } 
				  else { $subject_data = '<br>'; }
			}
			$tzcity = get_option('timezone_string');
			$tzoffset = get_option('gmt_offset');
			if ( ! empty($tzcity))  { 
			$current_time_timezone = date_create('now', timezone_open($tzcity));
			$timezone_offset =  date_offset_get($current_time_timezone);
			$website_timestamp = $submission_timestamp + $timezone_offset; 
			}
			else { 
			$timezone_offset =  $tzoffset * 3600;
			$website_timestamp = $submission_timestamp + $timezone_offset;  
			}
			   
			$website_date = date_i18n( get_option( 'date_format' ), $website_timestamp ) . ' ' . esc_html__('at', 'simpleform') . ' ' . date_i18n( get_option('time_format'), $website_timestamp );
			
			$last_message = '<div style="line-height:18px;">' . $from_data . '<b>'. esc_html__('Date', 'simpleform') .':</b>&nbsp;&nbsp;' . $website_date . $subject_data . '<b>'. esc_html__('Message', 'simpleform') .':</b>&nbsp;&nbsp;' .  $formdata['message'] . '</div>';


			$notification = ! empty( $settings['notification'] ) ? esc_attr($settings['notification']) : 'true';

			if ($notification == 'true') { 
				   
			$to = ! empty( $settings['notification_recipient'] ) ? esc_attr($settings['notification_recipient']) : esc_attr( get_option( 'admin_email' ) );
			
			$submission_number = ! empty( $settings['submission_number'] ) ? esc_attr($settings['submission_number']) : 'visible';
			
			$subject_type = ! empty( $settings['notification_subject'] ) ? esc_attr($settings['notification_subject']) : 'request';
			
			$subject_text = ! empty( $settings['custom_subject'] ) ? stripslashes(esc_attr($settings['custom_subject'])) : esc_html__('New Contact Request', 'simpleform');
			
			$subject = $subject_type == 'request' ? $request_subject : $subject_text;
				
			if ( $submission_number == 'visible' && empty($flagged) ):
				  $reference_number = $wpdb->get_var($wpdb->prepare("SELECT id FROM `$table_name` WHERE date = %s", $submission_date) );
				  $admin_subject = '#' . $reference_number . ' - ' . $subject;	
				  else:
				  $admin_subject = $flagged . $subject;	
			endif;

			$admin_message_email = '<div style="line-height:18px; padding-top:10px;">' . $from_data . '<b>'. esc_html__('Sent', 'simpleform') .':</b>&nbsp;&nbsp;' . $website_date . $subject_data  . '<br>' .  $formdata['message'] . '</div>'; 
			
			$headers = "Content-Type: text/html; charset=UTF-8" .  "\r\n";
			
		  
			do_action('check_smtp');
			add_filter( 'wp_mail_from_name', array ( $this, 'sform_notification_sender_name' ) ); 
			
			add_filter( 'wp_mail_from', array ( $this, 'sform_notification_sender_email' ) );
			
			$recipients = explode(',', $to);
			
			$sent = wp_mail($recipients, $admin_subject, $admin_message_email, $headers); 	
			remove_filter( 'wp_mail_from_name', array ( $this, 'sform_notification_sender_name' ) );
			remove_filter( 'wp_mail_from', array ( $this, 'sform_notification_sender_email' ) );

			if ($sent):
			  $mailing = 'true';
			endif;

		}
		

			
		$success_action = ! empty( $settings['success_action'] ) ? esc_attr($settings['success_action']) : 'message';    
		$thanks_url = ! empty( $settings['thanks_url'] ) ? esc_url($settings['thanks_url']) : '';    

		if( $success_action == 'message' ) { $redirect_to = esc_url_raw(add_query_arg( 'sending', 'success', $_SERVER['REQUEST_URI'] )); }
		else { $redirect_to = ! empty($thanks_url) ? esc_url_raw($thanks_url) : esc_url_raw(add_query_arg( 'sending', 'success', $_SERVER['REQUEST_URI'] )); }

		if ( ! has_filter('sform_post_message') ) { 
		  if ( $mailing == 'true' ) {
			 header('Location: '. $redirect_to);
			 ob_end_flush();
			 exit(); 
		  } 	 
		  else  { $error = 'server_error'; }
		}
		
		else { $error = apply_filters( 'sform_post_message', $form_id, $mailing ); 
		  if ( $error == '' ) {
			 header('Location: '. $redirect_to);
			 ob_end_flush();
			 exit(); 
		  } 	 
		  
		}
			 
		} 

		else  {  $error = 'server_error'; }
				
		}

		return $error;

    }
     
}
    
	
	/**
	 * Force "From Name" in Notification Email 
	 *
	 * @since    1.0
	 */
   
    public function sform_notification_sender_name() {
	    
     $id = isset( $_POST['form-id'] ) ? absint($_POST['form-id']) : '1';
	  
	 if ( $id == '1' ) { 
          $settings = get_option('sform_settings');
          $attribute = '[simpleform]';
          $shortcode_values = apply_filters( 'sform_form', $attribute );
          $form_name = ! empty($shortcode_values['name']) ? stripslashes(esc_attr($shortcode_values['name'])) : esc_attr__( 'Contact Us Page','simpleform'); 
     } else { 
          $settings_option = get_option('sform_'.$id.'_settings');
          $settings = $settings_option != false ? $settings_option : get_option('sform_settings');
          $attribute = '[simpleform id="'.$id.'"]';
          $shortcode_values = apply_filters( 'sform_form', $attribute );
          $form_name = ! empty($shortcode_values['name']) ? stripslashes(esc_attr($shortcode_values['name'])) : __( 'Contact Form','simpleform'); 
     }
     
     $sender = ! empty( $settings['notification_name'] ) ? esc_attr($settings['notification_name']) : 'requester';
     $custom_sender = ! empty( $settings['custom_sender'] ) ? esc_attr($settings['custom_sender']) : esc_attr( get_bloginfo( 'name' ) ); 

     if ( $sender == 'requester') { $sender_name = isset($_POST['sform-name']) ? sanitize_text_field($_POST['sform-name']) : esc_attr('Alex'); }
      
     if ( $sender == 'custom') { $sender_name = $custom_sender; }
    
     if ( $sender == 'form') { $sender_name = $form_name; }
     
     return $sender_name;
     
    }
  
	/**
	 * Force "From Email" in Notification Email 
	 *
	 * @since    1.0
	 */
   
    public function sform_notification_sender_email() {

     $id = isset( $_POST['form-id'] ) ? absint($_POST['form-id']) : '1';
	  
	 if ( $id == '1' ) { 
          $settings = get_option('sform_settings');
     } else { 
          $settings_option = get_option('sform_'.$id.'_settings');
          $settings = $settings_option != false ? $settings_option : get_option('sform_settings');
	 }
     
     $notification_email = ! empty( $settings['notification_email'] ) ? esc_attr($settings['notification_email']) : esc_attr( get_option( 'admin_email' ) );
      
     return $notification_email;
      
    }

	/**
	 * Force "From Name" in Confirmation Email 
	 *
	 * @since    1.0
	 */
   
    public function sform_confirmation_sender_name() {
	    
      $id = isset( $_POST['form-id'] ) ? absint($_POST['form-id']) : '1';
	  
	  if ( $id == '1' ) { 
          $settings = get_option('sform_settings');
      } else { 
          $settings_option = get_option('sform_'.$id.'_settings');
          $settings = $settings_option != false ? $settings_option : get_option('sform_settings');
	  }
	    
	  $sender_name = ! empty( $settings['autoresponder_name'] ) ? esc_attr($settings['autoresponder_name']) : esc_attr( get_bloginfo( 'name' ) ); 
	  
	  return $sender_name;
	  
    }

	/**
	 * Force "From Email" in Confirmation Email 
	 *
	 * @since    1.0
	 */
   
    public function sform_confirmation_sender_email() {
	
      $id = isset( $_POST['form-id'] ) ? absint($_POST['form-id']) : '1';
	  
	  if ( $id == '1' ) { 
          $settings = get_option('sform_settings');
      } else { 
          $settings_option = get_option('sform_'.$id.'_settings');
          $settings = $settings_option != false ? $settings_option : get_option('sform_settings');
	  }
	    
	  $from = ! empty( $settings['autoresponder_email'] ) ? esc_attr($settings['autoresponder_email']) : esc_attr( get_option( 'admin_email' ) );
	  
      return $from;
      
    }

} 
