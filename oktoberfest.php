<?php

/**
 *
 * Plugin Name:       Oktoberfest
 * Plugin URI:        http://ivorywebservices.com
 * Description:       Create a form for Oktoberfest.
 * Version:           1.0
 * Author:            Ivory Web Services
 * Author URI:        http://ivorywebservices.com
 * License:           GPL2
 * Text Domain:       oktoberfest
 *
 */

if ( ! defined( 'WPINC' ) ) { die; }

/**
 * Plugin constants.
 *
 * @since    1.0
 */
 
define( 'OKTOBERFEST_NAME', 'Oktoberfest' );
define( 'OKTOBERFEST_VERSION', '1.0' );
define( 'OKTOBERFEST_PATH', plugin_dir_path( __FILE__ ) );
define( 'OKTOBERFEST_URL', plugin_dir_url( __FILE__ ) );
define( 'OKTOBERFEST_BASENAME', plugin_basename( __FILE__ ) );
define( 'OKTOBERFEST_BASEFILE', __FILE__ );
define( 'OKTOBERFEST_ROOT', dirname( plugin_basename( __FILE__ ) ) );

/**
 * The code that runs during plugin activation.
 */
 
function activate_oktoberfest($network_wide) {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-activator.php';
	Oktoberfest_Activator::activate($network_wide);
}

/**
 * The code that runs during plugin deactivation.
 */
 
function deactivate_oktoberfest() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-deactivator.php';
	Oktoberfest_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_oktoberfest' );
register_deactivation_hook( __FILE__, 'deactivate_oktoberfest' );

/**
 * The core plugin class.
 */
 
require plugin_dir_path( __FILE__ ) . 'includes/class-core.php';

/**
 * Begins execution of the plugin.
 */
 
function run_Oktoberfest() {
	$plugin = new Oktoberfest();
	$plugin->run();
}

run_Oktoberfest();