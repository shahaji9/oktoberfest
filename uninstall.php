<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * @since      1.0
 */


// Prevent direct access. Exit if file is not called by WordPress.
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

// Confirm user has decided to remove all data, otherwise stop.
$settings = get_option('sform_settings');

if ( isset( $settings['deletion_data'] ) && esc_attr($settings['deletion_data']) == 'false' ) {
	return;
}

if ( !is_multisite() )  {

global $wpdb;

// Drop shortcodes table.

// Drop submissions table.
$wpdb->query( 'DROP TABLE IF EXISTS ' . $wpdb->prefix . 'fest_submissions' );

  
// Delete plugin options
$wpdb->query( "DELETE FROM {$wpdb->options} WHERE option_name LIKE 'oktoberfest\_%'" );
$wpdb->query( "DELETE FROM {$wpdb->options} WHERE option_name LIKE 'oktoberfest\-%'" );
$wpdb->query( "DELETE FROM {$wpdb->options} WHERE option_name LIKE '%\_oktoberfest\_%'" );

// Remove any transients we've left behind.
$wpdb->query( "DELETE FROM {$wpdb->options} WHERE option_name LIKE ('%\_transient\_oktoberfest\_%')" );

} 
