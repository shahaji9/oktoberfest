<?php

/**
 * The core plugin class
 *
 * @since      1.0
 */

class Oktoberfest {

	 
	protected $loader;

	protected $plugin_name;

	protected $version;

	public function __construct() {
		
		if ( defined( 'OKTOBERFEST_VERSION' ) ) { $this->version = OKTOBERFEST_VERSION; } 
		else { $this->version = '1.0'; }
		$this->plugin_name = 'Oktoberfest';
		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_public_hooks();
	}

	/**
	 * Load the required dependencies for this plugin.
	 */
	 
	private function load_dependencies() {

		// The class responsible for orchestrating the actions and filters of the core plugin.
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-loader.php';
		
		// The class responsible for defining all actions that occur in the admin area.		 
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-admin.php';
		
		// The class responsible for defining all actions that occur in the public-facing side of the site.		 
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-public.php';
		
		// The base class for displaying a list of submissions in an ajaxified HTML table.
        if ( ! class_exists( 'WP_List_Table' ) ) {
	    require_once ABSPATH . 'wp-admin/includes/class-wp-list-table.php';
        }
		
		// The customized class that extends the base class
        require_once plugin_dir_path( dirname( __FILE__ ) ) . '/includes/class-list-table.php';
						
		$this->loader = new Oktoberfest_Loader();

	}

	/**
	 * Register all hooks related to the admin area functionality of the plugin.
	 *
	 * @since    1.0
	 */
	
	private function define_admin_hooks() {

		$plugin_admin = new Oktoberfest_Admin( $this->get_plugin_name(), $this->get_version() );

		// Register the stylesheets for the admin area
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles', 1 );
		
		// Register the scripts for the admin area
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts', 1 );
		
		// Register the administration menu for this plugin into the WordPress Dashboard menu
	    $this->loader->add_action('admin_menu', $plugin_admin, 'fest_admin_menu' );
		

		
	    // Register ajax callback for shortcode attributes editing
	    $this->loader->add_action('wp_ajax_shortcode_costruction', $plugin_admin, 'shortcode_costruction');
		
	    // Register ajax callback for settings editing
	    $this->loader->add_action('wp_ajax_sform_edit_options', $plugin_admin, 'sform_edit_options');
		
 	    // Register callback for enabling smtp server
 		$this->loader->add_action( 'check_smtp', $plugin_admin, 'check_smtp_server' );
		
	    // Filter the tables to drop when a site into a network is deleted
        $this->loader->add_filter( 'wpmu_drop_tables', $plugin_admin, 'on_delete_blog' );
		
	    // Register ajax callback for include privacy page into label
	    $this->loader->add_action('wp_ajax_setting_privacy', $plugin_admin, 'setting_privacy');
		
		$this->loader->add_action( 'submissions_list', $plugin_admin, 'display_submissions_list', 10, 2 );
		
	}

	/**
	 * Register all hooks related to the public-facing functionality of the plugin.
	 *
	 * @since    1.0
	 */
	
	private function define_public_hooks() {

		$plugin_public = new Oktoberfest_Public( $this->get_plugin_name(), $this->get_version() );

		// Register the stylesheets for the public-facing side of the site
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		
		// Register the scripts for the public-facing side of the site
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		
		// Register shortcode via loader
        $this->loader->add_shortcode( "oktoberfest", $plugin_public, "fest_shortcode" );
		
	    // Register callback for form data validation
		$this->loader->add_filter( 'fest_form_validation', $plugin_public, 'formdata_validation', 12, 1 ); 
		
		$this->loader->add_filter( 'fest_download_image', $plugin_public, 'formdata_download', 13, 1 ); 
		
	    // Register callback for form data processing
		$this->loader->add_filter( 'fest_submit_form', $plugin_public, 'formdata_processing', 12, 2 );
		
        // Modify header information
		$this->loader->add_action( 'template_redirect', $plugin_public, 'ob_start_cache' );	
		
		// Register ajax callback for submitting form
	    $this->loader->add_action('wp_ajax_formdata_submit', $plugin_public, 'formdata_submit');
		
	    $this->loader->add_action('wp_ajax_nopriv_formdata_submit', $plugin_public, 'formdata_submit'); 

	    $this->loader->add_action('wp_ajax_formdata_download', $plugin_public, 'formdata_download');
		
	    $this->loader->add_action('wp_ajax_nopriv_formdata_download', $plugin_public, 'formdata_download'); 		
		
		$this->loader->add_action('wp_ajax_formdata_email', $plugin_public, 'formdata_email');
		
	    $this->loader->add_action('wp_ajax_nopriv_formdata_email', $plugin_public, 'formdata_email');
		
		$this->loader->add_action('wp_ajax_formdata_social_media', $plugin_public, 'formdata_social_media');
		
	    $this->loader->add_action('wp_ajax_nopriv_formdata_social_media', $plugin_public, 'formdata_social_media');
		
		
		$this->loader->add_action('wp_ajax_fest_file_upload', $plugin_public, 'fest_file_upload');
		
	    $this->loader->add_action('wp_ajax_nopriv_fest_file_upload', $plugin_public, 'fest_file_upload');

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0
	 */
	 
	public function run() {
		
		$this->loader->run();
		
	}

	/**
	 * Retrieve the name of the plugin.
	 *
	 * @since     1.0
	 */
	 
	public function get_plugin_name() {
		
		return $this->plugin_name;
		
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0
	 */
	 
	public function get_loader() {
		
		return $this->loader;
		
	}

	/**
	
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0
	 */
	 
	public function get_version() {
		
		return $this->version;
		
	}

}