<?php

/**
 * The class instantiated during the plugin activation.
 *
 * @since      1.0
 */
 
class Oktoberfest_Activator {

	/**
     * Run default functionality during plugin activation.
     */


    public static function activate($network_wide) {
	    
        self::oktoberfest_settings();
        self::create_db();
        self::create_folder();
    }

    /**
     *  Specify the initial settings.
     */
	 
	function create_folder() {
 
		$upload = wp_upload_dir();
		$upload_dir = $upload['basedir'];
		$upload_dir = $upload_dir . '/cam-img';
		if (! is_dir($upload_dir)) {
		   mkdir( $upload_dir, 0700 );
		}
	}

    public static function oktoberfest_settings() {
       
 	   $settings = get_option( 'oktoberfest_settings' );
       
       if ( !$settings ) {
	       
       $form_page = array( 'post_type' => 'page', 'post_content' => '[oktoberfest]', 'post_title' => __( 'Contact Us', 'oktoberfest' ), 'post_status' => 'draft' );
       $thank_string1 = __( 'Thank you for Submitting.', 'oktoberfest' );
       $thank_string2 = __( 'Your message will be reviewed soon, and we\'ll get back to you as quickly as possible.', 'oktoberfest' );
       $confirmation_img = OKTOBERFEST_URL . 'public/img/confirmation.png';
       $thank_you_message = '<div class="form confirmation"><h4>' . $thank_string1 . '</h4><br>' . $thank_string2 . '</br><img src="'.$confirmation_img.'" alt="message received"></div>'; 
       $confirmation_page = array( 'post_type' => 'page', 'post_content' => $thank_you_message, 'post_title' => __( 'Thanks!', 'oktoberfest' ), 'post_status' => 'draft' );
       $form_page_ID = wp_insert_post ($form_page);
       $confirmation_page_ID = wp_insert_post ($confirmation_page);
       $admin_email = get_option( 'admin_email' );
       $website_name = get_bloginfo( 'name' );
       $code_name = '[name]';
       $autoresponder_message = sprintf(__( 'Hi %s', 'oktoberfest' ),$code_name) . ',<p>' . __( 'We have received your request. It will be reviewed soon and we\'ll get back to you as quickly as possible.', 'oktoberfest' ) . '<p>' . __( 'Thanks,', 'oktoberfest' ) . '<br>' . __( 'The Support Team', 'oktoberfest' );          

       $settings = array(
	             'admin_notices' => 'false',
                 'ajax_submission' => 'false',
                 'spinner' => 'false',
	             'html5_validation' => 'false',
                 'javascript' => 'false',
                 'widget' => 'true',
                 'empty_fields' => __( 'There were some errors that need to be fixed', 'oktoberfest' ),
                 'characters_length' => 'false',
                 'empty_name' => __( 'Please provide your name', 'oktoberfest' ),
                 'incomplete_name' => __( 'Please type your full name', 'oktoberfest' ), 
                 'invalid_name' => __( 'The name contains invalid characters', 'oktoberfest' ), 
                 'name_error' => __( 'Error occurred validating the name', 'oktoberfest' ),                       
                 'empty_lastname' => __( 'Please provide your last name', 'oktoberfest' ),                 
                 'incomplete_lastname' => __( 'Please type your full last name', 'oktoberfest' ),                  
                 'invalid_lastname' => __( 'The last name contains invalid characters', 'oktoberfest' ), 
                 'lastname_error' => __( 'Error occurred validating the last name', 'oktoberfest' ),   
                 'empty_email' => __( 'Please provide your email address', 'oktoberfest' ),
                 'invalid_email' => __( 'Please enter a valid email', 'oktoberfest' ),  
                 'email_error' => __( 'Error occurred validating the email', 'oktoberfest' ),  
                 'empty_phone' => __( 'Please provide your phone number', 'oktoberfest' ),
                 'invalid_phone' => __( 'The phone number contains invalid characters', 'oktoberfest' ), 
                 'phone_error' => __( 'Error occurred validating the phone number', 'oktoberfest' ),      
                 'empty_subject' => __( 'Please enter the request subject', 'oktoberfest' ),
                 'incomplete_subject' => __( 'Please type a short and specific subject', 'oktoberfest' ), 
                 'invalid_subject' => __( 'Enter only alphanumeric characters and punctuation marks', 'oktoberfest' ),  
                 'subject_error' => __( 'Error occurred validating the subject', 'oktoberfest' ),                    
                 'empty_message' => __( 'Please enter your message', 'oktoberfest' ),
                 'incomplete_message' => __( 'Please type a clearer message so we can respond appropriately', 'oktoberfest' ),    
                 'invalid_message' => __( 'Enter only alphanumeric characters and punctuation marks', 'oktoberfest' ),
                 'message_error' => __( 'Error occurred validating the message', 'oktoberfest' ),
                 'consent_error' => __( 'Please accept our privacy policy before submitting form', 'oktoberfest' ),
                 'empty_captcha' => __( 'Please enter an answer', 'oktoberfest' ),
                 'invalid_captcha' => __( 'Please enter a valid captcha value', 'oktoberfest' ), 
                 'captcha_error' => __( 'Error occurred validating the captcha', 'oktoberfest' ), 
                 'honeypot_error' => __( 'Failed honeypot validation', 'oktoberfest' ), 
                 'server_error' => __( 'Error occurred during processing data. Please try again!', 'oktoberfest' ),
                 'duplicate_error' => __( 'The form has already been submitted. Thanks!', 'oktoberfest' ),
                 'ajax_error' => __( 'Error occurred during AJAX request. Please contact support!', 'oktoberfest' ),     
                 'success_action' => 'message',         
                 'success_message' => $thank_you_message, 
                 'confirmation_page' => '',        
                 'thanks_url' => '',
                 'server_smtp' => 'false',
                 'smtp_host' => '',
                 'smtp_encryption' => 'ssl',
                 'smtp_port' => '465',
                 'smtp_authentication' => 'true',
                 'smtp_username' => '',
                 'smtp_password' => '',
                 'notification' => 'true',
                 'notification_recipient' => $admin_email,
                 'bcc' => '',
                 'notification_email' => $admin_email,
                 'notification_name' => 'requester',
                 'custom_sender' => $website_name,
                 'notification_subject' => 'request',
                 'custom_subject' => __( 'New Contact Request', 'oktoberfest' ),
                 'notification_reply' => 'true',
                 'submission_number' => 'visible',
                 'autoresponder' => 'false', 
                 'autoresponder_email' => $admin_email,
                 'autoresponder_name' => $website_name,
                 'autoresponder_subject' => __( 'Your request has been received. Thanks!', 'oktoberfest' ),
                 'autoresponder_message' => $autoresponder_message,
                 'autoresponder_reply' => $admin_email,
                 'duplicate' => 'true',              
                 ); 
 
           add_option('oktoberfest_settings', $settings); 
	       
       }
       
       else { 
	       
	    $form_page_ID = ! empty( $settings['form_pageid'] ) ? esc_attr($settings['form_pageid']) : '';  
	    $confirmation_page_ID = ! empty( $settings['confirmation_pageid'] ) ? esc_attr($settings['confirmation_pageid']) : '';	  
	        
        if ( ! empty($form_page_ID) && get_post_status($form_page_ID) == 'trash' ) { 
	    wp_update_post(array( 'ID' => $form_page_ID, 'post_status' => 'draft' ));; 
	    }
	    
        if ( ! empty($confirmation_page_ID) && get_post_status($confirmation_page_ID) == 'trash' ) { 
	    wp_update_post(array( 'ID' => $confirmation_page_ID, 'post_status' => 'draft' ));; 
	    }
	    
       }
    }


    /**
     * Create custom tables.
     *
     * @since    1.0
     */
 
    public static function create_db() {

        global $wpdb;
        $charset_collate = $wpdb->get_charset_collate();
        $prefix = $wpdb->prefix;
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        
        $submissions_table = $prefix . 'fest_submissions';
        $sql = "CREATE TABLE " . $submissions_table . " (
            id int(11) NOT NULL AUTO_INCREMENT,
            name varchar(250) NOT NULL ,
            phone varchar(250) NOT NULL,
            email varchar(250) NOT NULL,
			total_time varchar(250),
			image varchar(250),
            date datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
            fest_message text NOT NULL,
			status tinytext,
            PRIMARY KEY  (id)
          ) ". $charset_collate .";";
          dbDelta($sql);
    }
      
}