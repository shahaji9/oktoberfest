(function( $ ) {
	'use strict';
	
	 $( window ).load(function() {
       
       $('.btn-delete').on('click', function(e){
		e.preventDefault();
        var id = $(this).attr('data-id');
		  
        var nonce = $('input[name="verification_nonce"]').val();
		if (confirm("You want to delete this row?")) {
  
		  $.ajax({
            type: 'POST',
            dataType: 'json',
            url: ajax_fest_settings_options_object.ajaxurl,
            data: {
              action: "setting_privacy",
              'verification_nonce': nonce,
	          'delete_id': id,
            },    
            success: function(data){
	          if( data.error === true ){
                $('#label-error').html('Error occurred during delete');
              }
	          if( data.error === false ){                
                $('#'+id).remove();
              }
            },
 			error: function(data){
              $('#label-error').html('Error occurred during delete');
	        } 	
		  });
		} 
		  return false;
	   });
       
   	 });

})( jQuery );