<?php

/**
 * Defines the admin-specific functionality of the plugin.
 *
 * @since      1.0
 */
	 
class Oktoberfest_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0
	 */
	
	private $plugin_name;

	/**
	 * The version of this plugin.
	 */
	
	private $version;

	/**
	 * Initialize the class and set its properties.
	 */
	
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

    /**
     * Register the administration menu for this plugin into the WordPress Dashboard menu.
     */  
     
    public function fest_admin_menu() {
	    
      $item = esc_html__('Oktoberfest', 'oktoberfest');           
      $item_menu = apply_filters( 'fest_notification_bubble', $item );
      $hook = add_menu_page(__('Oktoberfest', 'oktoberfest'), $item_menu,'activate_plugins','fest-submissions', array($this,'display_submissions_page'),'dashicons-email-alt', 24 );      
   
      global $fest_submissions_page;
      $fest_submissions_page = add_submenu_page('fest-submissions', esc_html__('Submissions','oktoberfest'), esc_html__('Submissions','oktoberfest'), 'activate_plugins', 'fest-submissions', array($this,'display_submissions_page'));

      global $settings_page;
      $settings_page = add_submenu_page('fest-submissions', esc_html__('Settings', 'oktoberfest'), esc_html__('Settings', 'oktoberfest'), 'manage_options', 'sform-settings', array($this,'display_settings_page'));

      do_action('load_submissions_table_options');
      do_action('fest_submissions_submenu');

   }
  
    /**
     * Render the submissions page for this plugin.
     *
     * @since    1.0
     */
     
    public function display_submissions_page() {
      
      include_once('partials/submissions.php');
   
    }

    /**
     * Render the editing page for this plugin.
     *
     * @since    1.0
     */
    
    public function display_editing_page() {
     
      include_once('partials/editing.php');
    
    }
    
    /**
     * Render the settings page for this plugin.
     * @since    1.0
     */
    
    public function display_settings_page() {
      
      include_once('partials/settings.php');
    
    }

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0
	 */
    
    public function enqueue_styles($hook) {
	    		
	 wp_register_style( 'fest-style', plugins_url('/css/fest-admin.css',__FILE__));
	 wp_register_style( 'fest-fancy', plugins_url('/css/jquery.fancybox.min.css',__FILE__));
	 
     
	 wp_enqueue_style('fest-style'); 
	 wp_enqueue_style('fest-fancy'); 
	      
	}
	
	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0
	 */
	
	public function enqueue_scripts($hook){
		
	wp_enqueue_script( 'fest_fancybox', plugin_dir_url( __FILE__ ) . 'js/jquery.fancybox.min.js', array( 'jquery' ), $this->version, false );
	    		
     wp_enqueue_script( 'sform_saving_options', plugin_dir_url( __FILE__ ) . 'js/fest-admin.js', array( 'jquery' ), $this->version, false );
	 
	 
	 
     wp_localize_script( 'sform_saving_options', 'ajax_fest_settings_options_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ), 'saving' => esc_html__( 'Saving data in progress', 'simpleform' ), 'loading' => esc_html__( 'Saving settings in progress', 'simpleform' ))); 
	      
	}
	
	/**
	 * Enable SMTP server for outgoing emails
	 *
	 * @since    1.0
	 */

	public function check_smtp_server() {
		
       $settings = get_option('sform_settings');
       $server_smtp = ! empty( $settings['server_smtp'] ) ? esc_attr($settings['server_smtp']) : 'false';
       if ( $server_smtp == 'true' ) { add_action( 'phpmailer_init', array($this,'sform_enable_smtp_server') ); }
       else { remove_action( 'phpmailer_init', 'sform_enable_smtp_server' ); }
   
   }

	/**
	 * Save SMTP server configuration.
	 *
	 * @since    1.0
	 */
	
    public function sform_enable_smtp_server( $phpmailer ) {
   
      $settings = get_option('sform_settings');
      $smtp_host = ! empty( $settings['smtp_host'] ) ? esc_attr($settings['smtp_host']) : '';
      $smtp_encryption = ! empty( $settings['smtp_encryption'] ) ? esc_attr($settings['smtp_encryption']) : '';
      $smtp_port = ! empty( $settings['smtp_port'] ) ? esc_attr($settings['smtp_port']) : '';
      $smtp_authentication = isset( $settings['smtp_authentication'] ) ? esc_attr($settings['smtp_authentication']) : '';
      $smtp_username = ! empty( $settings['smtp_username'] ) ? esc_attr($settings['smtp_username']) : '';
      $smtp_password = ! empty( $settings['smtp_password'] ) ? esc_attr($settings['smtp_password']) : '';
      $username = defined( 'SFORM_SMTP_USERNAME' ) ? SFORM_SMTP_USERNAME : $smtp_username;
      $password = defined( 'SFORM_SMTP_PASSWORD' ) ? SFORM_SMTP_PASSWORD : $smtp_password;
      $phpmailer->isSMTP();
      $phpmailer->Host       = $smtp_host;
      $phpmailer->SMTPAuth   = $smtp_authentication;
      $phpmailer->Port       = $smtp_port;
      $phpmailer->SMTPSecure = $smtp_encryption;
      $phpmailer->Username   = $username;
      $phpmailer->Password   = $password;

    }
	
    public function setting_privacy() {

      if( 'POST' !== $_SERVER['REQUEST_METHOD'] ) {	die ( 'Security checked!'); }
      if ( ! wp_verify_nonce( $_POST['verification_nonce'], "ajax-verification-nonce")) { exit("Security checked!"); }   
      if ( ! current_user_can('update_plugins')) { exit("Security checked!"); }   
      else { 
	  global $wpdb;
        $id = isset($_POST['delete_id']) ? absint($_POST['delete_id']) : 0;  
		$table_name = $wpdb->prefix . 'fest_submissions';
		$delete = $wpdb->delete( $table_name, array( 'id' => $id ) );
        
        if ( $delete ) {
				$label = "Deleted successfully";	    
				echo json_encode( array( 'error' => false, 'label' => $delete ) );
				exit;
        }   
        else {
           echo json_encode( array( 'error' => false ));
	       exit;
        }
        die();
      }

    }
	
}

	