<?php
if ( ! defined( 'WPINC' ) ) die;
?>

<div class="fest_submissions wrap">
<h1 class="backend"><span class="dashicons dashicons-email-alt"></span><?php esc_html_e( 'Submissions', 'oktoberfest' ); ?>


</h1>

<?php

global $wpdb; 
$table_name = "{$wpdb->prefix}fest_submissions"; 
//$results = $wpdb->get_results( "SELECT * FROM $table_name");

$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;      

$limit = 20; // number of rows in page
$offset = ( $pagenum - 1 ) * $limit;
$total = $wpdb->get_var( "select count(*) as total from $table_name" );
$num_of_pages = ceil( $total / $limit );

$results = $wpdb->get_results( "SELECT * from $table_name limit  $offset, $limit" );
$rowcount = $wpdb->num_rows;

?>
<?php wp_nonce_field( 'ajax-verification-nonce', 'verification_nonce'); ?>
<div class="festerror" id="label-error"></div>
<table class="submission-list" style="width: 100%; text-align: center">
<tr>
<th class="heading">No</th>
<th class="heading">Name</th>
<th class="heading">Email</th>
<th class="heading">Phone</th>
<th class="heading">Timing</th>
<th class="heading">Image</th>
<th class="heading">Action</th>
</tr>
<?php
$upload = wp_upload_dir();
$upload_dir = $upload['baseurl'];
$upload_dir = $upload_dir . '/cam-img/';
$i = ($pagenum*$limit)-($limit -1);
foreach( $results as $row ){
	echo "<tr id='".$row->id ."'>";
	echo "<td class='fest-no'>".$i."</td>";
	echo "<td class='fest-name'>".$row->name."</td>";
	echo "<td class='fest-email'>".$row->email."</td>";
	echo "<td class='fest-phone'>".$row->phone."</td>";
	echo "<td class='fest-time'>".$row->total_time."</td>";
	echo "<td class='fest-img'><a href='".$upload_dir . $row->image ."' data-fancybox ><img style='width:50px;' src='".$upload_dir . $row->image ."' alt='Img'></a></td>";
	echo "<td class='fest-action'><a class='btn-delete'  href='#' data-id='".$row->id ."' title='Delete'></a></td>";
	echo "</tr>";
	$i++;
}
?>
</table>
<?php
$page_links = paginate_links( array(
	'base' => add_query_arg( 'pagenum', '%#%' ),
	'format' => '',
	'prev_text' => __( '&laquo;', 'text-domain' ),
	'next_text' => __( '&raquo;', 'text-domain' ),
	'total' => $num_of_pages,
	'current' => $pagenum
) );

if ( $page_links ) {
	echo '<div class="tablenav" style="width: 99%;"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
}
?>
</div>